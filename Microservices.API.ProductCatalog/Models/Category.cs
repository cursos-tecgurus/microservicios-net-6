﻿using System;
using System.Collections.Generic;

namespace Microservices.API.ProductCatalog.Models
{
    public partial class Category
    {
        public Category()
        {
            Products = new HashSet<Product>();
        }

        public int Idcategory { get; set; }
        public string Name { get; set; } = null!;
        public string? Description { get; set; }
        public string? Guid { get; set; }

        //Lazy loading -> explicitamente invocar el get Productos
        public virtual ICollection<Product> Products { get; set; }
    }
}
