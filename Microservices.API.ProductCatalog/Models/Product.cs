﻿using System;
using System.Collections.Generic;

namespace Microservices.API.ProductCatalog.Models
{
    public partial class Product
    {
        public int Idproduct { get; set; }
        public string Name { get; set; } = null!;
        public string Description { get; set; } = null!;
        public decimal Price { get; set; }
        public string? Image { get; set; }
        public int Idcategory { get; set; }
        public string? Guid { get; set; }

        public virtual Category IdcategoryNavigation { get; set; } = null!;
    }
}
