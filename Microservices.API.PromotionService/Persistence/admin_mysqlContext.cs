﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microservices.API.PromotionService.Models;

#nullable disable

namespace Microservices.API.PromotionService.Persistence
{
    public partial class admin_mysqlContext : DbContext
    {
        public admin_mysqlContext()
        {
        }

        public admin_mysqlContext(DbContextOptions<admin_mysqlContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Promotion> Promotions { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
           
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Promotion>(entity =>
            {
                entity.HasKey(e => e.Idpromotion)
                    .HasName("PRIMARY");

                entity.ToTable("promotion");

                entity.Property(e => e.Idpromotion)
                    .HasColumnType("int(11)")
                    .HasColumnName("idpromotion");

                entity.Property(e => e.Discount)
                    .HasColumnType("decimal(7,2)")
                    .HasColumnName("discount");

                entity.Property(e => e.Max)
                    .HasColumnType("decimal(7,2)")
                    .HasColumnName("max");

                entity.Property(e => e.Min)
                    .HasColumnType("decimal(7,2)")
                    .HasColumnName("min");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("name");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
