﻿using Microservices.API.PromotionService.Models;
using Microservices.API.PromotionService.Persistence;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections;

namespace Microservices.API.PromotionService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PromotionsController : ControllerBase
    {

        private readonly admin_mysqlContext _context;

        public PromotionsController(admin_mysqlContext context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Promotion>>> GetPromotions()
        {
            return await _context.Promotions.ToListAsync();
        }

        [HttpGet("{amount}")]
        public async Task<decimal> GetDiscountByTotalAmount(decimal amount)
        {
            var promotion = await _context.Promotions.Where(p => amount >= p.Min && amount <= p.Max).FirstOrDefaultAsync();
            if (promotion != null) { 
                return promotion.Discount;
            } else
            {
                return 0;
            }
        }
    }
}
