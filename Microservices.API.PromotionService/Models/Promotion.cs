﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Microservices.API.PromotionService.Models
{
    public partial class Promotion
    {
        public int Idpromotion { get; set; }
        public string Name { get; set; }
        public decimal Min { get; set; }
        public decimal Max { get; set; }
        public decimal Discount { get; set; }
    }
}
