﻿using Microservices.API.ShoppingCart.ViewModels;
using Microservices.API.ShoppingCart.ViewModels.Responses;

namespace Microservices.API.ShoppingCart.Application
{
    public interface IOrderService
    {
        public OrderResponse SaveOrderAndDetails(List<PurchasedProduct> purchasedProducts);
    }
}
