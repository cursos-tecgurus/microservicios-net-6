﻿using Microservices.API.ShoppingCart.Models;
using Microservices.API.ShoppingCart.Persistence;
using Microservices.API.ShoppingCart.RemoteApplication;
using Microservices.API.ShoppingCart.RemoteModels;
using Microservices.API.ShoppingCart.ViewModels;
using Microservices.API.ShoppingCart.ViewModels.Responses;

namespace Microservices.API.ShoppingCart.Application
{
    public class OrderService : IOrderService
    {
        private readonly postgresContext _context;
        private readonly IProductsService _productsService;
        private readonly IPromotionService _promotionService;

        public OrderService(postgresContext context, IProductsService productsService, IPromotionService promotionService)
        {
            _context = context;
            _productsService = productsService;
            _promotionService = promotionService;
        }
        //Algoritmo a implementar https://pastebin.com/hLWwseyb y exponer en un controller
        public OrderResponse SaveOrderAndDetails(List<PurchasedProduct> purchasedProducts)
        {
            //1. Recorrer purchasedProducts
            //2. Si algun producto no lo encuentra regresar la respuesta en false (Producto no encontrado)
            //3. Sumar precio * cantidad comprada si todos fueron encontrados  
            //4. Total de la compra, consultar el descuento y aplicarlo.
            //5. Guardar Order y los detalles de la Orden
            OrderResponse response = new OrderResponse();
            response.Success = true;
            List<Product> products = new List<Product>();
            foreach (PurchasedProduct purchased in purchasedProducts)
            {
                var product = _productsService.findProductByGuid(purchased.Guid).Result;
                if (product == null)
                {
                    response.Success = false;
                    response.Message = "Producto no encontrado: " + purchased.Guid;
                    return response;
                } else
                {
                    products.Add(product);
                }
            }
                Order order = new Order();
                decimal totalAmount = 0;
                for (int i = 0; i < products.Count; i++)
                {
                    var product = products[i];
                    Orderdetail orderdetail = new Orderdetail();
                    orderdetail.Guidproduct = purchasedProducts[i].Guid;
                    orderdetail.Quantity = purchasedProducts[i].Quantity;
                    order.Orderdetails.Add(orderdetail);
                    totalAmount += (orderdetail.Quantity * product.Price);
                }
                decimal discount = _promotionService.GetDiscountByTotalAmount(totalAmount);

                order.Discount = discount;
                
                order.Totalprice = totalAmount - (totalAmount * discount / 100);
                response.Order = order;
                _context.Orders.Add(order);
                _context.SaveChanges();
                return response;
        }
    }
}
