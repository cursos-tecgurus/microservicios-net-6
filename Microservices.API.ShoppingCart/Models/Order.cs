﻿using System;
using System.Collections.Generic;

namespace Microservices.API.ShoppingCart.Models
{
    public partial class Order
    {
        public Order()
        {
            Orderdetails = new HashSet<Orderdetail>();
        }

        public int Idorder { get; set; }
        public DateTime Datetime { get; set; }
        public decimal Totalprice { get; set; }
        public decimal? Discount { get; set; }

        public virtual ICollection<Orderdetail> Orderdetails { get; set; }
    }
}
