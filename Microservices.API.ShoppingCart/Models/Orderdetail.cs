﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Microservices.API.ShoppingCart.Models
{
    public partial class Orderdetail
    {
        public int Idorderdetail { get; set; }
        public string Guidproduct { get; set; } = null!;
        public decimal Quantity { get; set; }
        public int Idorder { get; set; }

        [JsonIgnore]
        public virtual Order IdorderNavigation { get; set; } = null!;
    }
}
