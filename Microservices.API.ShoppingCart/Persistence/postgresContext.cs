﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microservices.API.ShoppingCart.Models;

namespace Microservices.API.ShoppingCart.Persistence
{
    public partial class postgresContext : DbContext
    {
        public postgresContext()
        {
        }

        public postgresContext(DbContextOptions<postgresContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Order> Orders { get; set; } = null!;
        public virtual DbSet<Orderdetail> Orderdetails { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
           
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Order>(entity =>
            {
                entity.HasKey(e => e.Idorder)
                    .HasName("order_pk");

                entity.ToTable("order");

                entity.Property(e => e.Idorder)
                    .HasColumnName("idorder")
                    .UseIdentityAlwaysColumn();

                entity.Property(e => e.Datetime)
                    .HasColumnName("datetime")
                    .HasDefaultValueSql("now()");

                entity.Property(e => e.Discount).HasColumnName("discount");

                entity.Property(e => e.Totalprice).HasColumnName("totalprice");
            });

            modelBuilder.Entity<Orderdetail>(entity =>
            {
                entity.HasKey(e => e.Idorderdetail)
                    .HasName("orderdetail_pk");

                entity.ToTable("orderdetail");

                entity.Property(e => e.Idorderdetail)
                    .HasColumnName("idorderdetail")
                    .UseIdentityAlwaysColumn();

                entity.Property(e => e.Guidproduct)
                    .HasMaxLength(100)
                    .HasColumnName("guidproduct");

                entity.Property(e => e.Idorder).HasColumnName("idorder");

                entity.Property(e => e.Quantity).HasColumnName("quantity");

                entity.HasOne(d => d.IdorderNavigation)
                    .WithMany(p => p.Orderdetails)
                    .HasForeignKey(d => d.Idorder)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("orderdetail_fk");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
