﻿namespace Microservices.API.ShoppingCart.ViewModels
{
    public class PurchasedProduct
    {
        public String Guid { get; set; }
        public int Quantity { get; set; }
    }
}
