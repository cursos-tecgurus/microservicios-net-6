﻿using Microservices.API.ShoppingCart.Models;

namespace Microservices.API.ShoppingCart.ViewModels.Responses
{
    public class OrderResponse
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public Order Order { get; set; }
    }
}
