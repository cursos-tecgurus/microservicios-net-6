﻿namespace Microservices.API.ShoppingCart.ViewModels.Requests
{
    public class OrderRequest
    {
        public OrderRequest()
        {
            PurchasedProducts = new List<PurchasedProduct>();
        }
        public List<PurchasedProduct> PurchasedProducts { get; set; }
    }
}
