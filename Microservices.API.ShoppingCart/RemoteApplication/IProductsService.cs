﻿using Microservices.API.ShoppingCart.RemoteModels;

namespace Microservices.API.ShoppingCart.RemoteApplication
{
    public interface IProductsService
    {
        Task<Product> findProductByGuid(string guid);
    }
}
