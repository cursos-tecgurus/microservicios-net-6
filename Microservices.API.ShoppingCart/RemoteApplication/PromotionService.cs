﻿using System.Net.Http;

namespace Microservices.API.ShoppingCart.RemoteApplication
{
    public class PromotionService : IPromotionService
    {
        private readonly IHttpClientFactory _httpClient;
        public PromotionService(IHttpClientFactory httpClient)
        {
            _httpClient = httpClient;
        }
        public decimal GetDiscountByTotalAmount(decimal amount)
        {
            var client = _httpClient.CreateClient("Promotions");
            var response = client.GetStringAsync($"api/Promotions/{amount}");
            if(response.IsCompletedSuccessfully)
            {
                return decimal.Parse(response.Result);
            } else
            {
                return 0;
            }
        }
    }
}
