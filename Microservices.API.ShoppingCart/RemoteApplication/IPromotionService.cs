﻿namespace Microservices.API.ShoppingCart.RemoteApplication
{
    public interface IPromotionService
    {
        public decimal GetDiscountByTotalAmount(decimal amount);
    }
}
