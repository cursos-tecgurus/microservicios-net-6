﻿using Microservices.API.ShoppingCart.RemoteModels;
using System.Text.Json;

namespace Microservices.API.ShoppingCart.RemoteApplication
{
    public class ProductsService : IProductsService
    {
        private readonly IHttpClientFactory _httpClientFactory;

        public ProductsService(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

        public async Task<Product> findProductByGuid(string guid)
        {
            var httpClient = _httpClientFactory.CreateClient("Products");
            var response = await httpClient.GetAsync($"api/Products/guid/{guid}");
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                //Cadena JSON a un objeto -> Deserealizacion
                var options = new JsonSerializerOptions() { PropertyNameCaseInsensitive = true };
                var producto = JsonSerializer.Deserialize<Product>(content, options);
                return producto;
            } else
            {
                return null;
            }

        }
    }
}
