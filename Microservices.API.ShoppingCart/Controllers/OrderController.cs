﻿using Microservices.API.ShoppingCart.Application;
using Microservices.API.ShoppingCart.ViewModels.Requests;
using Microservices.API.ShoppingCart.ViewModels.Responses;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Microservices.API.ShoppingCart.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly IOrderService _orderService;

        public OrderController(IOrderService orderService)
        {
            _orderService = orderService;
        }

        // POST api/<OrdersController>
        [HttpPost]
        public OrderResponse Post([FromBody] OrderRequest request)
        {
            return _orderService.SaveOrderAndDetails(request.PurchasedProducts);
        }
    }
}
