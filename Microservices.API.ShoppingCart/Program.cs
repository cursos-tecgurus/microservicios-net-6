using Microservices.API.ShoppingCart.Application;
using Microservices.API.ShoppingCart.Persistence;
using Microservices.API.ShoppingCart.RemoteApplication;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddDbContext<postgresContext>(opt => opt.UseNpgsql(builder.Configuration.GetConnectionString("PostgreSQL")));

//Registrar un servicio de tipo order
builder.Services.AddScoped<IOrderService, OrderService>();
builder.Services.AddScoped<IProductsService, ProductsService>();
builder.Services.AddScoped<IPromotionService, PromotionService>();

builder.Services.AddHttpClient("Products", httpClient =>
{
    httpClient.BaseAddress = new Uri(builder.Configuration["Services:Products"]);
});
builder.Services.AddHttpClient("Promotions", httpClient =>
{
    httpClient.BaseAddress = new Uri(builder.Configuration["Services:Promotions"]);
});

builder.Services.AddControllers(); //Escanear todas las clases hijas de BaseController

var app = builder.Build();

app.MapControllers(); //Mapear verbos http contra los controladores
// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

var summaries = new[]
{
    "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
};

app.MapGet("/weatherforecast", () =>
{
    var forecast = Enumerable.Range(1, 5).Select(index =>
        new WeatherForecast
        (
            DateTime.Now.AddDays(index),
            Random.Shared.Next(-20, 55),
            summaries[Random.Shared.Next(summaries.Length)]
        ))
        .ToArray();
    return forecast;
})
.WithName("GetWeatherForecast");

app.Run();

internal record WeatherForecast(DateTime Date, int TemperatureC, string? Summary)
{
    public int TemperatureF => 32 + (int)(TemperatureC / 0.5556);
}