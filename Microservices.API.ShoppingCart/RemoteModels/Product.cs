﻿namespace Microservices.API.ShoppingCart.RemoteModels
{
    public class Product
    {
        public decimal Price { get; set; }
    }
}
